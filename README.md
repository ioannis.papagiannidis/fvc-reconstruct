# FOAM `fvc::reconstruct` operator tests 

Testing the convergence of the `fvc::reconstruct` operator in OpenFOAM. 

## Authors

* **Tomislav Maric** 
* **Tobias Tolle**

## License

This project is licensed under the GPL3 License.  

## Getting Started

### Prerequisites

Install OpenFOAM, **any version should work** we have used: 

* OpenFOAM-plus, v1906

and related dependencies.

**gcc version with support for the standard "--std=c++2a" is required**

Tested with:

* gcc (GCC) 9.1.0

For the parameter variation, `PyFoam` is needed, it's available via `pip`

```
?> sudo pip install PyFoam  
```

For data processing and visualization 

* Jupyter notebook or Jupyter-lab


### Installing

Compile the application

```
?> cd code/foamTestFvcReconstruct/
?> wmake
```

## Running the tests

Run the hex2D parameter study that varies the mesh density to compute the convergence 

```
?> cd data/hex2D 
?> ./studyRun
```

This generates the output data, that is processed by the Jupyter notebook. Within `data/`, start Jupyter notebook (lab):

```
?> jupyter notebook  
```
## Contributing

The git repository is available on the following GitLab server: 

[https://git.rwth-aachen.de/leia/fvc-reconstruct](https://git.rwth-aachen.de/leia/fvc-reconstruct)

please use the issue tracker of the repo for reporting bugs. 
